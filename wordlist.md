- `asset n.资产`
- `origin n,起源`
- `observe v.观察`
- `attend v.出席`
- `recruit v.聘用`
- `guideline n.指导方针`
- `concert n.音乐会`
- `employee n.职工`
- `conduct v.组织`
- `resign v.辞职`
- `immense adj.巨大的`
- `share holder n.股东`
- `peerless adj.无与伦比的`
- `borrow v.借用`
- `odds n.几率`
- `engineer v.设计；n.工程师`
- `singular adj.非凡的`
- `consistent adj.连续的`
- `pursuit n.工作`
- `chief adj.级别最高的`
- `diverse adj.多种多样的`
- `material n.材料；adj.物质的`
- `absent v.缺席；adj.不在场的`
- `commercial adj.商业的`

---

- `encounter v.遇到；n.相遇`
- `stressed adj.有压力的`
- `role model 榜样`
- `inspire v.激励`
- `conscious adj.有意识的`
- `immediate adj.立即的`
- `sense vt.感到；n.感觉`
- `gossip n.流言蜚语`
- `urgent adj.急迫的`
- `indicate vt.表明`
- `reflect v.考虑，反映`
- `ordinary adj.普通的`
- `kite n.风筝`
- `toy n.玩具`
- `carpenter n.木匠`
- `wood n.木头`
- `bamboo n.竹子`
- `leaf n.叶子`

---

- `fierce adj.激烈的`
- `boost v.提高`
- `hinder v.阻碍`
- `opponent n.对手`
- `conflict v.冲突`
- `moral adj.道德的`
- `blind spot n.盲点`
- `contact v.接触`
- `rapid adj.快速的`
- `treat v.招待`
- `rival n.竞争者`
- `observation n.观察所得`
- `startup n.创业公司`
- `invest v.投资`
- `distinct adj.有区别的`
- `stake n.利害`
- `imply v.暗示`
- `protest v.抗议`
- `advocate v.拥护，提倡`
- `primarily adv.主要地`
- `confirm v.确认`
- `definite adj.明确的`
- `obtain v.获得，达到`
- `cite v.引用`
- `illustrate v.说明`
- `audience n.观众`
- `extend v.给予，扩大`

---

- `sign n.标志`
- `resident n.居住者；adj.定居的`
- `coat n.外套`
- `slogan n.口号`
- `treatment n.治疗`
- `prospect n.前景`
- `generational adj.代与代之间`
- `outnumber vt.在数量上超过`
- `overall adv.总的来说`
- `split v.分裂`
- `transformative adj.具有革新意义的`
- `engage v.吸引`
- `adapt v.适应`
- `adopt v.采用，收养`
- `reluctant adj.不情愿的`
- `mobility n.机动性`
- `pronounced adj.显著的`
- `emerge v.出现`
- `further v.促进`
- `widen v.变宽`
- `gap n.分歧`
- `routine n.常规`
- `rhythm n.节奏`
- `progress v.进步；n.前进，进步`
- `pace n.步调`
- `metabilism n.新陈代谢`
- `evolution n.进化`
- `meal n.餐，饭；vi.进餐`
- `custom n.习俗`
- `maintain v.维持`
- `give rise to 导致`
- `foundation n.基础`
- `supper n.晚饭`
- `extend v.延伸`
- `shrink to 压缩成`
- `stuff v.填塞`
- `bucket n.一桶（的量）`
- `spread over 分散`
- `tolerance n.宽容`
- `carry v.运输`
- `burden n.责任`
- `vigorous adj.有活力的`

---

- `accustom v.使习惯`
- `indispensable adj.不可缺少的`
- `accompany v.陪伴`
- `apparent adj.显然的`
- `solemn adj.庄严的`
- `sufficient adj.足够的`
- `mood n.心情`
- `violin n.小提琴`
- `qualification n.资格`
- `occasion n.时机`
- `severe adj.严峻的`
- `comparatively adj.相对地`
- `toast n.广受赞誉的人`
- `compliment n.恭敬；vt.向...道贺`
- `prosper n.繁盛`
- `yield n.产量`
- `complicated adj.复杂的`
- `reap v.收割`
- `climate n.气候`
- `coast n.海岸`
- `emit vt.发表，颁布`
- `excess n.超过；adj.超量的`
- `recession n.经济衰退`
- `vast adj.广阔的`
- `expense n.费用`
- `tuition n.学费`
- `dramatical adv.戏剧的`
- `essential adj.必要的`
- `depression n.沮丧`
- `fulfill v.履行`
- `attainment n.达到`
- `budget n.预算`
- `attendance n.出席`
- `drop v.下降`
- `current adj.最近的`
- `mutually adv.相互地`
- `consequently adv.因此`
- `feasible adj.可行的`
- `exclusive adj.不兼容的`
- `facility n.设备`
- `approach v.接近`
- `genuinely adv.真诚地`
- `interaction n.互动，相互影响`
- `involve v.使参与，专心于`

---

- `taps n.轻敲`
- `situatuin comedy 情景喜剧`
- `consultancy n.咨询`
- `cable n.有线电视`
- `rolling out 推出`
- `skepticism n.质疑`
- `prompting v.催促`
- `catalogue n.目录`
- `concrete adj.具体的`
- `budgets n.预算`
- `engage v.吸引`
- `campaign n.活动`
- `In theory 理论上`
- `amount n.数量`
- `count n.总数`
- `account n.账户`
- `agency n.代理`
- `reckons v.估计`
- `revenues n.收益`
- `triple n.三倍`
- `surged v.高涨`
- `novelty n.新奇`
- `rapidly adv.快速地`
- `harsh adj.刺耳的`
- `reform v.改良`
- `tempering v.锻炼`
- `hypotheses n.假设`
- `intolerant adj.无法忍受的`
- `emphasizing v.强调`
- `recession n.衰退`
- `maturity n.成熟`
- `mature adj.成熟的`
- `argued v.争论`
- `stimulate v.促进`
- `occupation n.工作`
- `occur v.发送`
- `slightly adv.轻微地`
- `interventions n.干涉`
- `especially adv.特别地`
- `bother v.打扰`
- `bleck adj.令人失望的`
- `On the contrary 相反`
- `inadequate adj.不充足的`
- `clarity n.清除、清晰度`
- `plunge v.骤降`
- `sector n.领域`
- `soar(ed) v.升高`
- `contradict v.反驳`
- `self-prochaimed adj.自称的`
- `identical adj.相同的`
- `defense v.防卫`
- `buildup v.组合`
- `fiscal adj.财政的`
- `stimulus n.刺激物`
- `eager adj.渴望的`
- `ideologically adv.意识形态上`
- `crippling out 使受损`
- `desperately adv.极度地`
- `corporate mismanagement 全体的管理不善`
- `insufficient demand 需求不足`
- `self-evident adj.不言而喻的`
- `irrational adj.不合理的`
- `thought-provoking adj.引人深思的`
- `groundless adj.莫须有的`
- `multiply v.相乘`
- `opposition n.反对`
- `feasible adj.可行的`
- `urgent adj.迫切的`
- `urgency n.紧迫`

---

- `capable adj.能干的`
- `moral compass 道德指南`
- `take on 承担`
- `manufacturer n.制造商`
- `social welfare 福利`
- `arise v.产生`
- `ascend v.上升`
- `combination n.组合`
- `advance n.进步`
- `in-home care 家庭护理`
- `the disabled 残疾人`
- `widespread adj.普遍的`
- `distract v.分心`
- `preparation n.准备`
- `bound n.界限;adj.一定的`
- `crash v.碰撞`
- `invade v.入侵`
- `privacy n.隐私`
- `run over 碾压`
- `jury n.陪审团`
- `sympathetic adj.同情的`
- `victims n.受害者`
- `punish someone with something 以某事处罚某人`
- `penalty n.惩罚`
- `innovation n.创新`
- `liable for something 对某事负责`
- `proximately adv.近似地`
- `preserve v.保留`
- `interfere v.干涉`
- `definite adj.明确的`
- `manifest adj.明显的;v.证明;n.货单`

---

- `escalate v.逐步上升`
- `fertilizer n.肥料`
- `irrigate v.灌溉`
- `fertilize v.施肥`
- `acres n.英亩`
- `cropland n.农田`
- `risks n.危害`
- `outweighed adj.超过`
- `gains n.利润`
- `urban adj.市内的`
- `consumer n.消费者`
- `potential adj.潜在的`
- `wastewater n.废水`
- `supply v.提供`
- `inexpensive adj.便宜的`
- `irrigation n.灌溉`
- `effectively adv.有效地`
- `agricultural adj.农业的`
- `absorb v.忍受`
- `bacteria n.细菌`
- `diseases n.疾病`
- `be attributed to 把..归于`
- `contaminated adj.污染的`
- `proper adj.适当的`
- `sanitation n.卫生设备`
- `be addressed with 被解决`
- `nutrient(s) n.化学品`
- `poverty n.贫穷`
- `consumption n.消费`
- `dry adj.干旱的`
- `regions n.地区`
- `scarce adj.缺乏的`
- `human waste n.粪便`
- `harvest v.收割`
- `lavatories n.厕所`
- `grain n.谷物`
- `crops n.农作物`
- `water-borne adj.水传播的`
- `alternative adj.替代的`
- `mud n.污泥`
- `chemical adj.化学的;n.化学品`
- `critical adj.关键的`
- `overly adv.过渡地`
- `overestimate v.高估`
- `estimate v.估计`
- `hazards n.危险`
- `contaminated adj.被污染的`
- `skeptical adj.怀疑的`
- `responsible adj.有责任的`
- `exaggerated adj.夸大的`
- `echo v.附和，回声`
- `crisis n.危机`
- `deem v.认为`
- `indepensable adj. 必要的`

---

- `graze on 吃`
- `high-cholesterol adj.高胆固醇的`
- `take-aways n.外卖`
- `ready-meals n.即食餐`
- `occasional adj.偶尔的`
- `vehicle n.工具`
- `celebrity n.名人`
- `the great hall 大厅`
- `medieval castle 中世纪的城堡`
- `five times 五倍`
- `In the year to … 截至某时`
- `Swedish n.瑞典`
- `furniture n.家具`
- `chain n.连锁店`
- `major adj.主要的`
- `minor adj.较小的`
- `staggering adj.难以置信的`
- `overhaul v.翻修`
- `exclusivity n.排他性`
- `reach v.到达`
- `selling point 卖点`
- `elevation n.高度`
- `elevated adj.升高的`
- `elevator n.升降机`
- `servant(s) n.仆人`
- `Right into … 刚刚步入某时`
- `smoky adj.多烟的`
- `aspiring adj.有抱负的`
- `middle classes n.中产阶级`
- `working classes n.工人阶级`
- `educated classes n.知识阶级`
- `prosper v.兴起`
- `natter v.唠叨`
- `pioneer(s) n.先驱者`
- `radical adj.激进的`
- `contemporary adj.当代的`
- `enhance v.提高`
- `promote v.促进`
- `observation n.观察`
- `borrow v.借鉴`
- `domestic adj.家庭的,国内的`
- `stove n.炉灶`
- `sink n.水池`
- `entirely adv.完全地`
- `modernist n.现代主义者`
- `triumph n.胜利`
- `tremendous adv.极大的`
- `duplicate v.复制`
- `exceed v.超过`
- `ranks among … 属于…之列`
- `in terms of 依照`

---

- `sound adj.健全的`
- `methodologies n.方法论`
- `explanation n.解释`
- `explain v.解释`
- `concise adj.简洁的`
- `contextual adj.上下文有关的`
- `step out in 走进`
- `engagement n.约定`
- `arena n.舞台`
- `incorporate v.合并,使纳入`
- `elected adj.当选的`
- `long-range adj.长期的`
- `investment n.投资`
- `devoted … to … 奉献于`
- `reap v.收获`
- `capability n.能力`
- `pride on … 以…为自豪`
- `hasten v.加速`
- `the lab bench n.实验台`
- `impairing v.损害`
- `convincing adj.有说服力的`
- `indulge v.迁就`
- `inertia n.[物]惯性,惰性`
- `warrant n.许可证`
- `climate n.气候`
- `agreement n.争论`
- `finalised v.定稿`
- `heralded v.预示`
- `era n.时代`
- `vital adj.至关重要的`
- `vitamin n.维他命`
- `vulnerable adj.易受影响的`
- `emission n.排放物`
- `reveal v.揭露`
- `injustice n.不公平`
- `essentially adv.根本上`
- `free-rider n.搭便车的人`
- `incur v.遭受`
- `enormously adv.巨大地`
- `fossi fuels n.化石燃料`
- `disproportionately adv.不均匀地`
- `On the flip side 反过来说`
- `despite prep.尽管`
- `scarcely adv.几乎不`
- `hail v.致敬`
- `sketch n.草稿`
- `sketchy adj.粗略的`
- `commendable adj.值得赞扬的`
- `pledge n.保证;v.保证`
- `lead up to … 作为...的准备`
- `deliver v.上交`
- `formal adj.正规的`
- `fund n.基金`
- `provide v.提供`
- `provision n.储备物资`
- `establish v.建立`
- `urgently adv.紧急地`
- `mobilisation n.动员`
- `emit v.发射,颁布`
- `tyrant n.暴君,专横的人`
- `critical adj.批评的`
- `cope with 应对`
- `hardly adv.几乎不`
- `consequence n.结果`
- `confront v.面对`
- `clarification n.说明`
- `realise v.了解到`
- `initiative n.主动性`
- `joint adj.共同的`
- `consensus n.一致认同`

---

- `depression n.抑郁`

- `anxiety n.焦虑`

- `suicide n.自杀`

- `alcohol n.酒精`

- `drug(s) n.药品`

- `brew v.即将发生`

- `psychiatric adj.精神的`

- `symptom(s) n.征兆`

- `in the extreme 极端地`

- `in jeopardy 在危险中`

- `dub v.起绰号`

- `falling through the cracks 深陷困境`

- `clinician(s) n.医生`

- `overlook v.忽视`

- `inconspicuous adj.不明显的`

- `excessive adj.过度的`

- `mental adj.精神的`

- `demonstrate v.证明`

- `in particular adv.尤其`

- `neglect v.忽视`

- `far behind v.落后`

- `catch off 措手不及`

- `obvious adj.明显的`

- `identification n.鉴别`

- `turn into 变成`

- `full-blown adj.成熟的`

- `disorder n.混乱`

- `resort v.求助于`

- `hitherto adv.至今`

- `psychological adj.精神的`

- `constitute v.构成`

- `liable adj.有责任的`

- `peer(s) n.同辈人`

- `shed v.流出`

- `trigger v.引发`

- `earning a living 谋生活`

- `seldom adv.很少`

- `trip n.旅行`

- `tourism industry n.旅行业`

- `upprecedented adj.前所未有的`

- `domestic travel 国内旅游`

- `travel abroad 国外旅游`

- `during the Nation Day holiday of 2016 2016年国庆节期间`

- `estimate v.估计`

- `overseas travel 境外旅游`

- `in the next few years 未来几年`

- `chain n.链子`

- `claim v.声称`

- `the following aspects 在接下来的方面`

- `adequate adj.足够的`

- `consideration n.考虑`

---

- `field(s) n.领域`
- `broadly adv.宽广地`
- `accelerate v.推动`
- `reluctant adj.不太愿意的`
- `geneticist n.遗传学家`
- `astronomer n.天文学家`
- `be accustomed to do something v.习惯于do something`
- `galaxies n.星系`
- `telescope n.望远镜`
- `the exception, the rule 例外, 惯例`
- `grant n.补助金`
- `assign v.分派`
- `journal(s) n.期刊`
- `agencies n.代理`
- `preserve n.独占的事物`
- `spring up 迅速成长`
- `demand v.要求`
- `cite v.引用`
- `moral adj.道德的`
- `plenty of 大量的`
- `citation(s) n.引用`
- `wood n.木材`
- `timber n.木材`
- `disciplined adj.遵守纪律的`
- `imperative adj.必要的`
- `patent(s) n.专利`
- `conducive v.有助于`
- `medical adj.医疗的`
- `ambiguous adj.模棱两可的`
- `liberal adj.慷慨的`
- `neutral adj.中立的`
- `hinder v.阻碍`
- `massive adj.大量的`
- `Macy 梅西`
- `sales n.销售额`
- `plunge v.下降`
- `cap v.脱帽致意,谢幕`
- `disappointing adj.令人失望的`
- `flagship stores n.旗舰店`
- `shrink v.缩水`
- `retailer n.零售商`
- `lure v.吸引`
- `dine v.进餐`
- `accessories n.饰品`
- `unseasonably adv.不合时宜地`
- `year-over-year 去年同期的`
- `decline v.下降`
- `be attributed to … 归于...`
- `forecast v.预算`
- `press n.新闻报道`
- `prompt v.导致`
- `temporary adj.短暂的`
- `thermometer n.温度计`
- `pledge v.许诺`
- `back-office 后勤的`
- `position n.职位`
- `implement v.实现,实施`
- `slash v.大幅削减`
- `fleet n.军舰,门店`
- `closure v.倒闭`
- `metropolitan adj.大都市的`
- `aggressively adv.激烈地`
- `compete with 竞争`
- `ambitious adj.有雄心的`
- `expansion n.扩张`
- `counter n.柜台`
- `bright adj.明亮的`
- `order v.订购`
- `sharply adv.突然地`
- `exceed v.超过`
- `merchandies n.商品`
- `dynasty n.朝代`
- `judge v.批评`
- `mutter v.喃喃自语`
- `verbal adj.口头的`
- `clues n.想法`
- `trigger v.激发`
- `lip(s) n.嘴唇`
- `sealed adj.密封的`
- `keep one's lips sealed 缄口不言`
- `utter v.说出`
- `hunt v.打猎`
- `pace n.步调`
- `apparently adv.明显地`
- `matured adj.成熟的`
- `brilliance n.才华`
- `refute v.反驳`
- `augment v.增强`
- `tone(s) n.音调`
- `grocery n.杂货店`
- `volume n.音量`
- `arrogance n.自大`
- `dedicate adj.奉献`
- `incur v.蒙受,招致`
- `instruct v.指导`
- `obscurely adv.不清楚地`
- `spectator(s) n.观众`

---

- `revealed v.表明`

- `phenomenon n.现象`

- `the left hemisphere n.左半球`

- `stimulate v.刺激`

- `laboratory n.实验室`

- `demonstrate v.证明`

- `drown v.淹死`

- `look out for v.提防`

- `prodator(s) n.捕食者`

- `consciously adv.有意识地`

- `dramatically adv.戏剧性地`

- `exotic adj.奇异的`

- `inherent adj.内在的,固有的`

- `marine adj.海洋的`

- `varieties n.多样性`

- `enrolled v.就读`

- `ballet n.芭蕾舞`

- `soccer n.足球`

- `two parents 双亲`

- `hectic adj.忙碌的`

- `extended family n.大家庭`

- `beaten up 被打`

- `rearing v.抚养`

- `widen v.加宽`

- `socioeconomic adj.社会经济的`

- `ethical adj.合乎道德的`

- `compassionate adj.富有同情心的`

- `cultivation n.养育`

- `supervision n.监督`

- `figures n.人物`

- `compliant adj.兼容的`

- `affluent adj.富有的`

- `discipline v.处罚`

- `technique(s) n.技巧`

- `obedient adj.听话的`

- `punishment n.惩罚`

- `veteran n.老将`

- `nicotine n.尼古丁`

- `addict n.瘾君子`

- `testify v.作证`

- `fancy v.想像;n.幻想;adj.花样的`

- `packaging n.包装`

- `strip v.脱掉`

- `charities n.慈善机构`

- `the Labour party n.劳工党`

- `legislate v.立法`

- `appeal n.吸引力`

- `plain adj.简朴的`

- `cigarettes n.香烟`

- `inviting adj.诱人的`

- `tobacco n.烟草`

- `the addictive poison 会上瘾的毒品`

- `intervene v.干预`

- `banning v.取缔`

- `roughly adv.大致地`

- `halve v.对半分`

- `premature adj.过早的`

- `measure n.措施,测量`

- `tame v.控制,驯服`

- `fraction n.一小部分`

- `intention n.意图`

- `consult v.咨询`

- `suspend v.推迟,暂停`

- `lobbying firm n.游说公司`

- `the prime minister n.总理`

- `deny v.否认`

- `adviser n.顾问`

- `interest(s) n.利益`

- `proceed v.继续`

- `parliament n.议会`

- `member n.成员`

- `dissolve v.解散,溶解`

- `March n.三月`

- `authorise v.授权`

- `trade n.贸易`

- `overwhelmingly adv.压倒性地`

- `amendment(s) n.修正案`

- `bill n.账单,议案`

- `regulate v.调整`

- `ingredient n.成分`

- `intervention n.干涉`

- `bizarra adj.奇异的`

- `chocolate n.巧克力🍫`

- `orange n.橙子🍊`

- `fuel n.燃料;v.加剧`

- `obesity n.肥胖`

- `reluctantly adv.不情愿的`

- `over-cautiousness n.过度谨慎`

- `sensible adj.明智的`

- `political adj.政治的`

- `sustained adj.持续的`

- `rid v.摆脱`

- `take up 接受`

- `subsidise v.资助`

- `adopt v.采用`

- `decline v.下降`

- `sharply adv.突然地`

- `considerably adv.相当地`

- `substitute(s) n.替代品`

- `reluctant adj.不情愿的`

- `be addicted to something 对 something 上瘾`

- `controversy n.争议`

- `obese adj.肥胖的`

- `ingredient(s) n.成分`

---

- `vanish v.消失`
- `diverse adj.多种多样的`
- `tension n.紧张`
- `shore(s) n.海滨`
- `colonial adj.殖民地的`
- `distinctive adj.独特的`
- `resist v.抵制`
- `homogenization n.同质化`
- `beard n.胡子`
- `laundry n.洗衣房`
- `neat adj.整齐的`
- `dry v.晒干`
- `brim n.边缘`
- `terminal(s) n.终点站`
- `industrialized adj.工业化的`
- `moderate v.减弱`
- `perceive v.把…看作`
- `relic(s) n.文物`
- `inflexible adj.固定的`
- `dedicate v.致力于`
- `convenience n.方便`
- `quarrel n.争议`
- `inconvenient adj.不方便的`
- `conscientious adj.有良心的`
- `wartime n.战争时期`
- `thrift n.节俭`
- `virtues n.美德`
- `destined adj.注定的`
- `process n.过程`
- `progress v.进步`
- `respective adj.各自的`
- `universal adj.全体的`
- `undergone v.经历`
- `debt n.债务`
- `annual adj.年度的`
- `fees n.费用`
- `barely adv.仅仅`
- `guarantee n.保证`
- `decent adj.体面的`
- `non-graduate adj.不需要大学文凭的`
- `denounce v.抨击`
- `elaborate adj.精心编造的`
- `repayment n.偿还`
- `threshold n.阈值`
- `loan(s) n.贷款`
- `maintenance n.保养`
- `grant(s) n.补助金`
- `Yet it still pays to go to university 上大学仍然是值得的`
- `tuition n.学费`
- `portion n.一部分`
- `wirte off 勾销`
- `seminar(s) n.研讨会`
- `extraordinarily adv.异常地`
- `circle v.盘旋`
- `elite adj.上等的`
- `i.e. 例如`
- `upper-second adj.上部二等的`
- `school-leaver(s) n.中学毕业生`
- `investion n.投资`
- `moan v.悲叹`
- `embrace v.欣然接受`
- `prestige n.威望`
- `bolster v.支持`
- `outsource n.外包`
- `facility n.设施`
- `interal adj.内部的`
- `effort n.努力`
- `proposal n.提议`
- `privatize v.私人化`
- `validate v.使有效`
- `immaterial adj.无关紧要的`
- `institution n.机构`
- `initiative n.主动性;adj.自发的`
- `advocate v.提倡`
- `opt v.选择`
- `finalize v.定稿`
- `examine v.审查`
- `justification n.辩解`
- `deliberate v.商讨`
- `specific(s) n.详情`
- `resignation n.辞职`
- `flatly adv.直截了当地`
- `neglect v.不重视`
- `faculty n.院系`
- `reveal v.揭露`

---

- `rice n.水稻🌾`
- `wheat n.小麦`
- `bean(s) n.豆类`
- `fundamental adj.基本的`
- `anable land n.耕地`
- `population n.人口`
- `date back to 7700 years ago 回到7700年前`
- `prior adj.先前的`
- `chemical fertilizer n.化肥`
- `organic adj.有机的`
- `accomplish v.完成`
- `sustainable development n.可持续发展`
- `Brazil n.巴西`
- `forthcoming adj.即将到来的`
- `speech n.演讲`
- `the Bible n.圣经`
- `slave n.奴隶`
- `candles n.蜡烛🕯`
- `church n.教堂⛪️`
- `enthusiastic adj.热情的`
- `recipient(s) n.接受者`
- `nap v.打盹`
- `greenhorn n.新手`
- `cow n.母牛`
- `ox n.公牛`
- `jail n.监狱`
- `prison n.监狱`

---

- `biological adj.生物的`

- `diversity n.多样性`

- `phase out 逐步废除`

- `institute n.机构`

- `demonstrate v.证明`

- `genetic adj.基因`

- `ancestor n.祖先`

- `ancestry n.起源`

- `variation(s) n.差异`

- `paradox n.悖论`

- `illuminate v.照亮`

- `medical adj.医疗的`

- `clinical adj.简朴的`

- `chunk n.大块`

- `underdiagnose v.漏诊`

- `specific adj.详细的`

- `precisely adv.精确地`

- `individual n.个体; adj.个人的`

- `population n.群体`

- `discrimination n.歧视`

- `negligence n.不作为`

- `refrain from doing something 克制做某事`

- `irrelevant adj.无关紧要的`

- `examine v.检查`

- `term(s) n.术语`

- `cling v.紧抓`

- `racism n.种族主义`

- `prolong v.延长`

- `dramatic adj.戏剧性的`

- `endorsement n.认可`

- `trumpet v.宣告`

- `equitable adj.合理的`

- `specialized adj.专业的`

- `creep v.爬行`

- `tipping point 引爆点, 临界点`

- `forward-looking adj.有远见的`

- `rooftop n.屋顶`

- `panel(s) n.嵌板`

- `impressive adj.令人印象深刻的`

- `trivial adj.平凡的`

- `poised adj.沉着的`

- `emerging adj.新兴的`

- `rural adj.乡下的`

- `bypass v.避开`

- `pole(s) n.杆`

- `illuminate v.照亮`

- `gigawatt-hours n.兆瓦时`

- `decidedly adv.明确地`

- `pedestrian adj.平淡无奇的; n.行人`

- `enabling n.使用性`

- `disrupting n.侵扰性`

- `portable adj.便于携带的`

- `the centralized electric grid 中央电网`

- `prosumer(s) 产消合一者`

- `ultimately adv.最终`

- `dominate v.控制`

- `sophisticated adj.复杂的`

- `diverse adj.不同的, 多种多样的`

- `innovation n.革新`

- `turn out 表明`

---

- `specify v.具体说明`
- `literally adv.真实地`
- `climatic adj.气候的`
- `envelope n.信封；包裹物`
- `humidity n.湿度`
- `seedling n.树苗🌱`
- `age v.生长`
- `offspring n.后代`
- `rely v.依靠`
- `exclusively adv.特定地`
- `walnut oak pine n.核桃树 n.橡树 n.松鼠`
- `scatter v.分散`
- `hoard v.积蓄`
- `subsequently adv.随后`
- `retrieve v.取回`
- `hide up 储藏`
- `species n.种类`
- `notably adv.尤其`
- `habitat n.栖息地`
- `vulnerable adj.易受攻击的`
- `fruitful adj.硕果累累的`
- `withdraw v.撤退`
- `legacy n.遗产`
- `trial n.试验; adj.试验的`
- `hasty adj.急忙的`
- 
