# 《1368个单词就够了》笔记

 Table of Contents

  1. 名词
    - [具体名词（355个）](#具体名词)
    - [抽象名词（166个）](#抽象名词)
    - [构成类名词（65个）](#构成类名词)
    - [属性类名词（70个）](#属性类名词)
  2. 动词
    - [单纯型动词（114个）](#单纯型动词)
    - [世故型动词（219个）](#世故型动词)
  3. [形容词（208个）](#形容词)
  4. [介词（48个）](#介词)
  5. [副词及其他（123个）](#副词及其他)

 ## 具体名词

  具体名词：背下来就可以

  总单词量：355个

  类别：38个

  ### 有关物的名词

  #### 时间类

   **`time`**, `moring`, `afternoon`, `noon`, `evening`, `night`, `tonight`, `today`, `tomorrow`, `yesterday`, `weekend`, `month`, `year`, `season`, `spring`, `autumn`, `winter`, `day`, `date`, `moment`, `period`, `history`, `future`, `birthday`, `holiday`, `festival`, `schedule`, `age`.

  #### 气候类

   `weather`, `rain`, `snow`, `wind`, `cloud`.

  #### 自然类

   `nature`, `mountain`, `air`, `light`, `water`, `fire`, `ice`, `smoke`, `heat`, `ground`, `sky`, `river`, `field`, `forest`, `sea`, `stone`, `star`.

  #### 植物类

   **`plant`**, `grass`, `tree`, `crop`.

  #### 动物类

   **`animal`**, `bird`, `cat`, `dog`, `horse`, `rabbit`, `elephant`, `bear`, `tiger`, `lion`, `bull(公牛)`, `cow(母牛)`, `pig`, `chicken`, `fish`, `sheep`, `monkey`, `snake`, `reptile(爬行动物)`.

  #### 昆虫类
   **`insect`**, `bee`, `butterfly`, `spider`.

  #### 其他生物

   `bacteria(细菌)`.

  #### 地点类

   **`place`**, `hospital`, `restaurant`, `hotel`, `university`, `factory`, `jail(监狱)`, `zoo`, `park`, `school`, `store`, `club`, `bar`, `court(法院)`, `market`, `town`, `village`, `city`.

  #### 机构类

  **`organization`**, `company`, `charity`.

  #### 声音类

   **`sound`**, `voice`, `noise`, `music`.

  #### 食品类

   **`food`**, `egg`, `meat(肉类)`, `steak(牛排)`, `bread`, `cake`, `dessert`, `soup`, `sandwich`, `noodle`, `pie`, `chocolate`, `sauce(调味酱)`.

  #### 餐饮类

   **`meal`**, `breakfast`, `lunch`, `dinner`.

  #### 蔬菜类

   **`vegatable`**, `potato`, `tomato`, `carrot(胡萝卜)`, `lettuce(植莴苣)`, `bean(豆科植物)`.

  #### 水果类

   **`fruit`**, `apple`, `orange`, `banana`.

  #### 食材类

   **`ingredient(原料)`**, `oil`, `sugar`, `salt`, `butter(黄油)`, `cream(奶油)`.

  #### 饮料类

   **`drink`**, `coffee`, `milk`, `tea`, `juice`, `beer`, `wine`, `alcohol`.

  #### 营养类

   **`nutrient(营养品)`**, `mineral(矿物)`, `vitamin(维他命)`, `protein(蛋白质)`.

  #### 疾病类

   **`disease`**, `fever(发烧)`, `flu(感冒)`, `cancer`.

  #### 房屋类

   `house`, `office`, `room`, `floor`, `wall`, `window`, `door`, `roof`, `kitchen`.

  #### 家具类

   **`furniture(家具)`**, `bed`, `chair`, `desk`, `table`, `seat`, `couch(长沙发)`.

  #### 服装类

   `clothes`, `shirt`, `shoes`, `cap(帽状物)`, `hat`, `coat`, `dress`, `pants`, `uniform(制服)`, `suits`, `underwear`, `pocket(口袋)`, `jeans(牛仔裤)`, `button`, `zip(拉链)`, `sock(袜子)`.

  #### 首饰类

   **`jewelry(首饰)`**, `diamond(钻石)`, `ring`.

  #### 化妆品

   **`cosmetics(化妆品)`**, `perfume(香水)`, `lipstick(口红)`, `powder(粉末)`.

  #### 交通类

   `transport(运输)`, `traffic(交通)`, `bicycle`, `ship`, `boat`, `plane`, `train`.

  #### 车辆类

   **`vehicle(车辆)`**, `motorcycle`, `bus`, `car`, `truck`, `brake(刹车)`, `engine`, `gear`, `tire(轮胎)`, `wheel`.

  #### 路桥类

   `road`, `street`, `bridge`, `station`.

  #### 装置类

   **`device`**, `lock`, `key`, `bell`, `scale(秤)`, `tap(阀门)`.

  #### 设备类

   **`equipment`**, `shower`, `radio`, `telephone`, `camera`, `computer`, `TV`, `fridge(冰箱)`, `video`.

  #### 容器类

   **`container`**, `box`, `cup`, `dish(碟)`, `plate(盘)`, `bowl(碗)`, `basket(篮)`, `tub(桶)`, `sink(洗碗槽)`, `pot(壶)`, `pan(锅)`.

  #### 材料类

   **`material`**, `cloth(布料)`, `paper`, `glass`, `plastic(塑料)`, `gold`, `paint(油漆)`, `chemical`.

  #### 日常用品类

   `toy`, `brush(刷子)`, `mirror`, `chain`, `board(木板)`, `handle`, `cartoon`, `bottle`, `wood`, `clock`, `knife(刀)`, `pen`, `book`, `album`, `menu`, `card`, `envelope(信封)`, `ticket(票)`, `pipe(管道)`, `tube(管筒)`, `wire`, `belt(皮带)`, `web(蜘蛛网)`, `film`, `screen`, `gift`, `channel`, `alarm(警报器)`, `electricity`, `towel(毛巾)`, `carpet(地毯)`, `tool`, `junk(垃圾)`, `fork`, `spoon(匙)`, `medicine(药品<内服>)`, `drug(麻醉药品)`, `pill(药丸💊)`

  #### 数量

   `couple`, `double`, `load(大量)`, `pack(小包)`, `lot`.

  #### 单位

   **`unit`**, `meter(米)`, `inch(英寸)`, `pound(磅)`, `gram(克)`, `piece`.

  #### 事情

   `thing`, `issue`, `matter`, `stuff`, `object`, `event`.

  ### 有关人的名词

  #### 家庭角色类

   `parent`, `daughter`, `son`, `father`, `mother`, `brother`, `sister`, `husband`, `wife`, `uncle`, `aunt`, `grandfather`, `grandmother`, `role`.

  #### 社会角色类

   `kid`, `child`, `baby`, `boy`, `girl`, `man`, `woman`, `friend`, `lady`, `gentleman`, `boss`, `customer`, `student`, `neighbor`, `person`, `valunteer`, `fool`, `sir`, `madam`.

  #### 职业类

   **`job`**, `doctor`, `nurse`, `professor`, `layer`, `engineer`, `teacher`, `coach`, `guard(警卫)`, `judge`.

  #### 群体类

   `group`, `people`, `class`, `team`, `human`, `staff`, `society`, `generation`, `army`, `government`, `family`.

## 抽象名词
抽象名词：光背下来还不够

总单词量：166个

类别：9个

#### 话语类
`words`, `topic`, `subject(主题)`, `joke`, `question`, `suggestion`, `instruction(说明书)`, `permission(允许)`.

#### 文字类
`document`, `story`, `report`, `novel`, `note`, `text`, `letter`, `email`, `blog`, `list`, `menu`.

**rule**: `lay`, `grammar`, `principle`.

#### 信息类
`information`, `message`, `news`, `update`, `notice(通知)`, `fact`, `detail`, `evidence`, `clue(提示)`, `background`, `data`, `sign(标志)`, `knowledge`.

#### 状态类
`state`, `condition`, `environment`.

#### 行为类
`act`, `exercise`, `practice`, `test`, `experiment`, `lesson`, `bath(洗澡)`, `security`, `attention`, `focus`, `step`, `education`, `advertisement`, `treatment`, `trick`, `habit`, `service`, `business`, `homework`.

**job**: `task`, `project`, `challenge`.

**duty**: `responsibility`, `fault`.

**performance**: `concert(🎵音乐会)`, `opera(歌剧)`, `drama(戏剧)`.

**study**: `math`, `art`, `science`, `philosophy`, `psychology(心理学)`.

**arrangement**: `deal`, `insurance`, `account`, `engagement(婚约)`.

**occasion**: `meeting`, `interview`, `party`, `picnic(野餐)`, `wedding`, `funeral(葬礼)`, `barbecue(烧烤)`, `ceremony`.

**money**: `profit(利润)`, `income`, `salary`, `loan(借款)`, `cash`, `cost`, `credit`, `deposit(押金)`, `price`, `tax(税)`, `bill`, `budget(预算)`.

#### 活动类
`activity`, `game`, `discipline(训练)`, `crime`, `trade`, `economy`, `politics`, `war`, `trip`, `fashion`, `industry`.

**sports**: `soccer`, `football`, `golf(高尔夫🏌)`, `basketball`, `race`.

#### 思想类
`thought`, `idea`, `view`, `theory`, `religion(宗教)`.

**plan**: `strategy`, `policy`, `program`.

#### 整体类
`network`, `internet`, `series`, `career`.

**system**: `language`, `transport`.

#### 其他类
`favor`, `privilege(特权)`, `mistake`, `risk`, `advantage`, `benefit`, `problem`, `trouble`, `surprise`, `secret`, `award`, `prize`, `diet`, `property`, `option`, `case`, `example`, `sample`, `score`, `limit`, `experience`, `effort`, `energy`, `figure`.

## 构成类名词
构成类名词：你的外在于内在

总单词量：65个

类别：8个

#### 核心词
`part`

##### 万物皆可分 part
虽然有些事物的组成部分会有自己专门的名称,但几乎所有的事物都是可以用part这个词来分析表述。学会把事物分成不同的part,有助于我们更加轻松地
描述和分析事物。我们看看下面的实例。

###### 人体组成可分 part
It's part of the body. 然后再说嘛这个器官是干什么的就可以了。同样的表达也可以用在动物和植物身上。

###### 生活用品可分 part
🥚鸡蛋分两个部分，一个是蛋白，另一个是蛋黄，你只要说是 yellow part 还是 white part 就可以。猪肉有不同的部位，🎂蛋糕切成不用的块，也是不同的 part。一台💻电脑或机器会由很多的零部件组成，这也是 part。

###### 区域可分 part
中国分华东、华南等不同的区，上海分徐汇、静安等不同的区，一个大卖场会分食品、家电等不同的区，这些都是不同的 part。

###### 机构分 part
###### 文学艺术分 part
###### 团队分 part
###### 时间分 part
###### 事件分 part
###### 工作分 part
###### 生活分 part

使用 part 代替以下词：

organ, segment, fragment, slice, portion, wedge, compartment, fraction, component, accessory, region, district, quarter, sector, division, passage, paragraph, section, episode.

#### 部位
`side`, `edge`, `base`, `core`.

##### 谁都有不为人知的一面：side
像万物皆有自己的 part 一样，任何事物也都有自已的 side 。有句俗话叫：千人千面。这儿的面就是指 side。

一个平面有两个 side，如一张纸、一枚硬币、一个煎蛋🍳等。而一个立体则可能有几个不同的 side，如爬山，你可以选择从不同的 side 上山。

side除了指面，也指边。像一张纸或长条桌子除了正反两个面，两个长的边,也叫side。而一条路的 side 既可指路边，比如说 Park your car at the side of road。也可指路的两面。

#### 身体
`head`, `eye`, `ear`, `face`, `nose`, `foot`, `mouth`, `tooth`, `tongue`, `neck`, `shoulder`, `breast(乳房)`, `chest(胸部)`, `stomach(腹部)`, `arm(臂)`, `hand`, `finger`, `nail`, `leg`, `knee(膝盖)`, `throat(喉咙)`, `brain`, `heart`, `lung`, `muscle`, `bone`, `nerve`, `hair`, `skin`, `blood`, `sweat(汗)`.

#### 精神
`mind`, `spirit`, `soul`, `emotion`.

**feeling**: `shame(羞耻)`, `stress`, `respect`, `desire(期望)`, `pain`, `sense`.

#### 力量
`strength`, `force`.

**power**: `authority(权威)`.

**ability**: `sight`, `intelligence`, `memory`, `skill`.

#### 动物
`tail`, `wing`.

#### 植物
`flower`, `leaf`, `root`, `branch(枝条)`.

#### 其他
`whole`, `rest`, `half`.

## 属性类名词
属性类名词：你的状态

总单词量：70个

类别：12个

### 静态属性

#### 形状
**`shape`**, `appearance`.

#### 尺寸
**`size`**, `space`.

#### 位置
**`point`**, **`line`**, **`end`**.

**position**: `bottom`, `middle`, `top`, `front`, `back`, `center`, `corner`, `left`, `right`.

**direction**: `east`, `west`, `north`, `south`.

#### 品质
`quality`, `feature`, `character`, `personality`.

#### 其他属性
`color`

`weight`

`tone(音调)`

`material`

`structure`

#### 数量
**`amount`**, `number`, `measurement`, `distance`, `volume`, `angle(角度)`, `temperature`.

**level**: `degree`, `grade`, `rank`, `standard`.

**rate**: `speed`, `percentage`.

#### 类别
`kind`, `type`

### 动态属性

#### 方式
`form`

**way**: `behavior`, `mood`, `logic`, `tradition`, `culture`, `style`, `pattern`, `relation`, `manner(方式)`.

#### 原因
`cause`, `reason`, `source`, `excuse`, `factor`.

#### 目的
`purpose`, `goal`.

#### 结果
`effect`, `result`.

#### 过程
`course`, `process`.

## 单纯型动词
单纯形动词：广交天下朋友，可与介词相互组合构成词组。

总单词量：114个

类别：15个

要求：resquest/ask for

报复：revenge/get back on

调查：investigate/look into

合作：cooperate/work together

取出：withdraw/take out

建立：establish/set up

削减：decrease/cut down

组装：assemble/put together

拒绝：reject/turn down

归还：return/give back

加速：accelerate/speed up

第一种是单独一个词（世故型动词），第二种是一个词组。词组中的第一个词（单纯型动词）表示动作，第二个词（介词）表示结果。

“一笑置之”怎么说？😰

`laugh it off`

laugh 表示的动作，off 表示的是结果。

“被人识破”怎么说？😰

`see through`

see 是动作，through 表示结果。


### 本源词
`be`, `go`, `come`, `make`.

###### 动词的鼻祖：be, go, make

###### 任何东西都可以 make 出来
make food, make coffee, make clothes, make a house, make a car, make a boby, make fire, make noise, make space, make time, make a story, make a rule, make a living, make a website

事物已经 make 出来了，那就是 be 了。“人类”这个词在英语中有个表达叫 human being，human 是指“人”，而所有的生物都可以统称 being，外星人👽也可以称之为一种 being。being 还有一种解释叫 existence（存在的意思）。所以 be 就是“存在”的意思。

I am a student.

He is tall.

The book is on the table.

意思就是说某样东西是如何存在的，以什么形式存在，在哪里存在，等等。而不仅仅解释为“是”的意思。

事物已经 make 出来了，它们已经存在（be)了，但存在的事物是不可能一直保持一种状态的，任何事物都在发展变化，那就是 go 出场的时候了。

1. 与不同类别的名词搭配，表达不同的行为动作。

    When does the train go?(火车可以 go)

    How did your interview go?(面试可以 go)

    The house won't go for less than $200,000.(房子可以 go)

2. 可以与所有表示方向与位置的介词搭配，代替不同的动词。

    You'll never get mom to go along with your idea.(go along 代替 agree)

    Once you've made the decision, I'm afraid there's no going back.(go back 代替 regret)

3. 可以与大多数形容词搭配，表达不同属性的变化。

    The milk had gone sour.(牛奶🥛酸了)

    As always, everything went wrong at the last minute.(像以前一样，所有的事情在最后一刻功亏一篑)

### 归属

最普遍的事物关系：have, give, get, take

#### 拥有某物 `have`

#### 得到某物 `get`

#### 给出某物 `give`

#### 拿到某物 `take`

### 终点位置

#### 使到达某一位置 `put`

#### 使轻轻到达某一位置 `lay`, `place`

#### 使从其他位置到达说话者位置 `bring`

#### 使从说话者到达其他位置 `send`

#### 使位置暂时改变 `hold`

#### 使位置保持不变 `keep`

#### 位置保持不变 `stay`, `hang`, `settle`

### 相互位置

#### 一事物到另一事物上面 `over`

#### 一事物到另一事物下面 `support`

#### 两事物混合 `max`

#### 两事物交换 `switch`

#### 两事物相连 `link`, `join`, `connect`, `stick`, `tie`

#### 两事物相遇 `meet`

#### 两事物交叉 `cross`

### 运动轨迹

#### 从一个点到另一个点 `move`, `shift`

#### 经过某个点 `pass`

#### 低点往高点 `raise`, `lift`, `rise`

#### 高点往低点 `drop`, `fall`

#### 一事物在另一事物后面 `follow`

#### 一事物在另一事物前面 `lead`

#### 一物载另一物 `carry`

#### 往复运动 `bounce(弹)`, `roll`, `shake`, `screw(拧)`

### 速度

#### 快速运动 `run`, `rush`, `hurry`

### 力量

#### 轻轻接触 `touch`

#### 用力接触 `hit`, `knock`, `crash`

#### 使快速运动 `throw`

#### 用力接触并来回移动 `rub`

#### 用力使事物脱离原来的位置 `pull`

#### 对事物朝某一方向用力 `push`, `draw`, `press`

### 空间

#### 空间开发或关闭 `open`, `close`, `shut`

#### 一事物进入某一空间 `fit`, `enter`

#### 一事物(气体或液体)进入某一空间 `fill`, `pump(抽取)`, `spill(洒)`, `spray(喷)`

#### 事物离开某一空间 `clean`, `clear`, `wipe(抹掉)`

#### 事物离开某一地点 `leave`, `quit`

#### 一事物到达某一地点 `arrive`, `reach`

#### 一事物固定在某一空间 `fit`, `set`

### 时间

#### 某事发生 `begin`, `start`, `happen`

#### 某事结束 `finish`

#### 某事停止 `stop`

### 方向

#### 一个方向到另一个方向 `turn`

### 整体与部分

#### 从大到小(用力) `break`, `crack(破裂)`, `split`, `tear(撕开)`

#### 从大到小 `divide`

#### 从多到少 `cut`

### 形状
`bend(弯曲)`, `fold`, `twist(扭曲)`, `stretch(拉长)`, `spread(展开)`.

### 尺寸
`grow`.

### 行为
`look`, `see`, `listen`, `hear`, `say`, `speak`, `talk`, `ask`, `think`, `believe`, `know`, `like`, `love`, `do`.

### 系动词
`become`, `seem`, `feel`, `sound`, `smell`, `taste`.

## 世故型动词
世故型动词：朋友少，但效率高

总单词量：219个

类别：14个

###### “盐”就是“腌”的意思

英语中还有一类比较常见的世故型动词，那就是直接把名词拿来用作动词。

比如 rain，即可以当名词用表示“雨水”，也可以当动词用表示“下雨🌧”。dream，即可以当名词用表示“梦”，也可以当动词用，意思是“做梦”。

其实很好理解，“腌”东西不就得用盐嘛！所以，你就可以说成：

Salt the fish.(把鱼🐟腌起来)

类似的名词非常多，举例如下：

Water the plants.

He colored his dog red.

He pocketed the changes.

He is milking the cow.

Please list your ten favorite songs.

类似的名词动用还有很多，如下：

ice, oil, paper, parent, program, tax, wall, head, chest, shoudler, elbow, position, angle, cap, cash, detail, heat, license, sugar, shape, sun, air, bag, boss.

### 看
`find`, `watch`, `observe`, `ignore`, `search`, `show`.

### 说
意见：`agree`, `cancel`, `let`, `allow`.

告诉：`claim`, `warn`, `introduce`, `explain`, `express`, `cinfirm`.

要求：`beg`, `charge(控告)`, `order`.

带有情感地说：`argue(争论)`, `blame`, `praise(称赞)`, `encourage`, `complain`, `promise`, `insist`, `shout(叫喊)`, `threat(威胁)`

其他：`answer`, `call`, `count`, `pronounce`.

### 想
`consider`, `guess`, `deserve(值得)`, `offer`,

`doubt`, `suspect(推测)`, `trust`, `forget`, `remember`, `imagine`, `mean`,

`learn`, `understand`, `wonder`,

`decide`, `try`, `want`, `hope`, `wish`, `expect`,

`will`, `shall`, `should`, `may`, `might`, `can`, `must`, `dare`, `need`,

`impress(印)`, `attract`.

### 感觉
`enjoy`, `suffer`, `appreciate`, `care`, `hate`, `worry`, `thank`, `welcome`, `bless`, `thrill(激动)`, `freak(异常兴奋)`, `shock`, `bother`, `annoy`, `disturb(打扰)`.

### 自然现象
`die`, `live`, `burn`, `boil`.

### 人体行为
肢体：`dance`, `ride`, `lie`, `climb`, `beat(跳动)`.

上肢：`hug`.

下肢：`sit`, `stand`, `walk`, `kick`, `jump`, `skip`.

面部：`smile`, `laugh`, `cry`.

嘴巴：`eat`, `kiss`, `suck(吸)`, `lick(舔)`, `blow(吹)`, `cough(咳)`, `sing`.

鼻子：`breathe`.

皮肤：`bleed(流血)`

精神：`sleep`, `wake`, `relax`.

健康：`hurt`, `injure`, `cure`, `inject(注射)`.

### 生活行为
`cook`, `bake(烘烤)`, `fry(油炸)`, `drive`, `measure`, `wear`, `wash(洗)`, `tape(捆绑)`, `record`, `post`, `travel`, `screw`, `dig`, `hide`, `pack`, `mark`.

### 社会行为
`buy`, `sell`, `shop`, `pay`, `spend`, `waste`, `invite`, `rent`, `fight`, `kill`, `steal(偷)`, `cheat(骗)`, `invest(投资)`, `rob`, `wait`, `marry`, `divorce(离婚)`, `borrow(借入)`, `lend(借出)`, `owe(欠)`, `vote`, `bet`, `celebrate`,

`play`, `pretent(假装)`,

`help`, `direct`, `entertain(招待)`, `interrupt(打断)`, `distract(分散)`,

`design`, `build`, `invent`, `copy`,

`prepare`, `organize`, `manage`, `handle`, `treat`, `control`,

`use`, `apply`, `save`, `choose`, `accept`, `pick`, `collect`,

`win`, `fail`, `miss`, `lose`,

`check`, `examine(调查)`, `compare`, `solve`, `analyze`.

### 运动
`skate(滑行)`, `swim`, `fly`, `shoot`.

### 工作
`work`, `interview`, `hire`, `fire`, `compete`, `retire`.

### 学习
`read`, `write`, `spell`, `translate`.

### 事物之间的关系
`match`, `qualify(合格)`, `equal`, `share(分配)`, `include`, `separate`, `belong`, `depend`, `involve(牵涉)`.

### 事物的发展变化
`change`, `develop`, `improve`, `reduce`, `add`, `promote`.

### 事物相互作用
`damage`, `spoil(损坏)`, `ruin`, `affect`, `attack`.

## 形容词
让感觉飞起来：形容词

总单词量：208个

类别：47个

### 物的属性与构成
大小：`big`, `huge`, `little`, `small`, `medium`.

高低：`high`, `low`.

长短：`long`, `short`, `tall`.

深浅：`deep`, `shallow`.

粗细：`thick`, `thin`, `fat`, `slim`.

宽窄：`narrow`, `wide`.

重量：`heavy`, `light`.

正斜：`straight`, `curved`, `flat`.

形状：`round`, `square`.

距离：`near`, `far`.

事物与空间：`fall`, `empty`, `blank`, `bare(赤裸的)`, `dirty`.

时间：`new`, `fresh`.

数量：`extra`, `only`, `single`, `poor`, `rich`, `slight(微不足道的)`, `total`.

质地：`hard`, `soft`, `tough(棘手的)`, `tender(嫩的)`, `smooth(光滑的)`, `rough`, `sharp`, `blunt(钝的)`, `fresh`, `raw`, `pure`, `plain(朴素的)`, `even(平坦的)`.

力量：`strong`, `weak`, `tight`, `loose`, `firm`, `tense(紧绷的)`.

状态：`liquid(液体)`, `gas`, `solid`.

温度：`hot`, `warm`, `cold`, `cool`.

湿度：`dry(干的)`, `wet`.

亮度：`bright`, `dark`, `dull(暗淡的)`.

味道：`sweet`, `bitter(苦的)`, `delicious`, `sour`, `spicy(🌶辣的)`.

声音：`loud`, `quiet`.

### 人的属性与构成
时间：`young`, `old`.

身体：`hungry(饿的)`, `ill`, `sick`, `tired`, `blind(盲的)`, `sore(疼痛的)`, `born(天生的)`, `pregnant`, `alive`.

行为：`busy`, `violent`, `wild(不寻常的)`.

外表：`beautiful`, `ugly`, `sexy`.

心智：`smart`, `clever`, `stupid`, `confused`, `awake`, `asleep`, `familiar`, `patient(忍耐的)`.

态度：`polite`, `lazy`, `honest`, `rude`, `brave`, `aggressive`.

情感：`interested`, `curious`, `proud`, `sure`, `confident`.

心情：`happy`, `glad(高兴的)`, `sad`, `upset`, `sorry`, `guilty`, `calm`, `afraid`, `angry`, `crazy`, `mad`, `excited`, `bored`, `disappointed`, `jealous`, `lonely`.

### 事的属性与构成
 难度：`easy`, `hard`, `difficult`.

 时间：`late`, `due(预期的)`, `urgent(迫切的)`, `efficient(生效的)`.

 可能性：`possible`, `available`

 安全性：`dangerous`, `safe`.

 其他：`wrong`, `strict`, `correct`, `proper(合适的)`, `lucky`, `fair`, `successful`.

 ### 综合属性
 品质(好)：`good`, `nice`, `fine`, `great`, `perfect`, `wonderful`, `amazing`, `excellent`.

 品质(坏)：`bad`, `terrible`, `awful`.

 真实性：`real`, `true`, `false`, `fake`.

 完整性：`complete`.

 精确性：`exact`, `specific`.

 复杂性：`complicated`, `simple`.

 其他：`fun`, `horrible(可怕的)`, `weird(奇怪的)`, `strange`, `comfortable`, `incredible`, `gross(不雅的)`.

 ### 事物关系
 特殊性：`special`, `regular`.

 一致性：`same`, `different`.

 普遍性：`typical`, `normal`, `common`, `general`, `popular`, `average`, `particular`, `own`.

 重要性：`serious`, `causal`, `important`, `main`, `formal`, `professional`.

 必要性：`necessary`.

 关联性：`free`, `relative`, `legal`, `physical`, `mental`, `local`, `native`, `international`, `cheap`, `expensive`, `separate`, `public`, `worth`.


## 介词
英语的灵魂：介词

总单词量：48个

类别：14个


### 动态位置
轨迹：`across`, `along`, `past`, `over`, `up`, `down`, `through`, `on`, `off`, `in`, `out`, `against`.

起点：`from`.

终点：`to`, `toward`, `for`, `about`.

### 静态位置
点：`at`.

点与点：`by`, `beside`, `before`, `after`, `behind`, `between`, `around`.

点与平面：`beyond`, `under`, `above`, `below`.

点与空间：`outside`, `inside`, `within`.

从属：`of`, `with`, `without`,

时间：`during`, `since`, `till`.

替代：`instead of`.

比较：`than`, `as`.

整体与部分：`besides`, `among`, `except`, `including`.

因果：`according to`

其他：`despite(尽管)`, `per`.

## 副词及其他
英语的配角：副词及其他
总单词量：123个
类别：9个

### 副词
时间：`now`, `early`, `just`, `then`, `recently`, `ago`, `already`, `yet`, `ever`, `never`, `forever`.

位置：`here`, `there`.

方向：`away(离开)`, `forth(向前)`, `apart(相隔)`, `together`, `aside`.

数量：`extra`, `alone`.

程度：`quite`, `much`, `very`, `well`, `extremely`, `almost`, `enough`.

强调：`actually`, `especially`, `certainly`.

速度：`fast`, `slow`, `soon`, `suddenly`, `immediately`, `gradually`.

频率：`again`, `often`, `usually`, `always`.

顺序：`finaly`, `eventually`.

转折：`otherwise`.

可能性：`probably`, `perhaps`, `maybe`.

递进：`also`, `too`, `either`, `neither`, `else`.

引导：`when`, `where`, `how`, `why`, `what`.

### 连词
因果：`because`.

转折：`but`, `thought`.

并列：`and`.

条件：`if`, `unless`, `except`.

时间：`while`.

选择：`or`, `whether`.

### 限定词
顺序：`last`, `next`.

数量：`all`, `any`, `some`, `both`, `each`, `either`, `neither`, `every`, `few`, `many`, `much`.

指代：`this`, `that`, `these`, `those`, `another`, `such`.

代词：`I`, `you`, `he`, `she`, `we`, `they`, `it`.

疑问代词：`which`, `who`.

冠词：`a`, `an`, `the`.

感叹词：`bye`, `hello`, `no`, `yes`, `pardon(宽恕)`, `please`, `wow`, `damn`.

缩略词：`Mr.`, `Ms.`.

数词：`zero`, `one`, `two`, `three`, `four`, `five`, `six`, `seven`, `eight`, `nine`, `ten`, `hundred`, `thousand`, `million`, `billion`.